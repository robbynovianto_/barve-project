@extends('layout/navbar')
<!-- CSS -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
</style>
    <title>Barve - Admin</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/aos.css">
        {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
        <link rel="stylesheet" href="../assets/css/bootstrap.min_2.css">
        <link rel="stylesheet" href="../assets/css/bootstrap-min.css">
        <link rel="stylesheet" href="../assets/css/sweetalert2.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/vendor.bundle.base.css">
<main id="main">
    <div class="pageHeader">
        <div class="pageHeaderLayer">
            <div class="container">
                <h1>Login</h1>
                <p>Silakan melakukan login menggunakan user dan password yang Anda miliki untuk dapat mengakses semua fitur BARVE.</p>
            </div>
        </div>
    </div>
<section id="contact" class="section-bg">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="form form-login">
                    <form class="php-email-form" action="{{route('postlogin')}}" method="post">
                        @csrf
                        <h1><i class="bi bi-key"></i></h1>
                        <p>Login</p>
                        <div class="form-group">
                            @error('email')
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Email</strong> yang anda gunakan salah.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                             @enderror
                            <input type="text" class="form-control" name="email" placeholder="e-mail" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="password" required>
                        </div>
                        <button type="submit"><i class="bi bi-box-arrow-in-right"></i> Masuk</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
<!-- Footer -->
@extends('layout/footer')

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<div id="preloader"></div>

<script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/aos/aos.js" type="text/javascript"></script><script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/bootstrap-5/js/bootstrap.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/waypoints/noframework.waypoints.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/plugin/sweetalert2/sweetalert2.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/js/main.js" type="text/javascript"></script></body>
</html>

