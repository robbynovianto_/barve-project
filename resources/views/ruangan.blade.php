@extends('layout/navbar')
<!-- CSS -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
</style>
    <title>Barve - Admin</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/aos.css">
        {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
        <link rel="stylesheet" href="../assets/css/bootstrap.min_2.css">
        <link rel="stylesheet" href="../assets/css/bootstrap-min.css">
        <link rel="stylesheet" href="../assets/css/sweetalert2.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/vendor.bundle.base.css">
<main id="main">
    <div class="pageHeader">
        <div class="pageHeaderLayer">
            <div class="container">
                <h1>Pesan Ruangan</h1>
                <p>
                Anda dapat melakukan pemesanan ruangan untuk kegiatan rapat melalui halaman ini. 
                Silakan melakukan pemesanan ruangan dengan memilih ruangan, tanggal dan waktu pelaksanaannya.</small></p>
            </div>
        </div>
    </div>
<section class="crslRoom">
    <div class="container">
        <input type="hidden" id="select-jam-mulai">
        <input type="hidden" id="select-jam-selesai">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="searchForm">
                <form action="pemesanan" method="get">
                    <label>Pilih Tanggal Ketersediaan Ruangan</label>
                    <div class="input-group input-group-sm">
                        <input type="date" class="form-control" placeholder="Tentukan tanggal " id="set_date"
                                name="tanggal">
                        <button class="btn btn-outline-primary" type="submit"><i class="bi bi-search"></i> Lihat
                            Jadwal
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Fitur -->
<section id="services">
    <div class="container aos-init aos-animate" data-aos="fade-up">
        <header class="section-header wow fadeInUp">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <h3>PESAN RUANGAN</h3>
                </div>
            </div>
        </header>
        @foreach($ruangan as $ruangans)
            <div class="row">
                <div class="col-md-6">
                    <img src="http://bmnkita.id/amara//images/room/1613551553_a70c798d8247fd96578c.jpg" width="600" height="400">
                </div>
                <div class="col-md-6">
                    <h1>{{$ruangans->nama_ruangan}}</h1>
                    <p class="mb-2">Samping kanan Ruang Rapat Paripurna Gedung Nusantara V</p>
                    <p class="mb-2">Kapasitas : {{$ruangans->kapasitas}}</p>
                    <p class="mb-1">
                        <ol>
                            @php($fasilitas = explode(".",$ruangans->fasilitas))
                                @foreach($fasilitas as $fas)
                                    <li>{{$fas}} </li>
                                @endforeach
                        </ol>                                    
                        
                        <hr>
                    </p>
                    <!-- <div class="clearfix">
                        <div class="timeList "><a href="javascript:void(0)">07:00</a></div><div class="timeList "><a href="javascript:void(0)">07:30</a></div><div class="timeList "><a href="javascript:void(0)">08:00</a></div><div class="timeList "><a href="javascript:void(0)">08:30</a></div><div class="timeList "><a href="javascript:void(0)">09:00</a></div><div class="timeList "><a href="javascript:void(0)">09:30</a></div><div class="timeList "><a href="javascript:void(0)">10:00</a></div><div class="timeList "><a href="javascript:void(0)">10:30</a></div><div class="timeList "><a href="javascript:void(0)">11:00</a></div><div class="timeList "><a href="javascript:void(0)">11:30</a></div><div class="timeList "><a href="javascript:void(0)">12:00</a></div><div class="timeList "><a href="javascript:void(0)">12:30</a></div><div class="timeList "><a href="javascript:void(0)">13:00</a></div><div class="timeList "><a href="javascript:void(0)">13:30</a></div><div class="timeList "><a href="javascript:void(0)">14:00</a></div><div class="timeList "><a href="javascript:void(0)">14:30</a></div><div class="timeList "><a href="javascript:void(0)">15:00</a></div><div class="timeList "><a href="javascript:void(0)">15:30</a></div><div class="timeList "><a href="javascript:void(0)">16:00</a></div><div class="timeList "><a href="javascript:void(0)">16:30</a></div><div class="timeList "><a href="javascript:void(0)">17:00</a></div><div class="timeList "><a href="javascript:void(0)">17:30</a></div><div class="timeList "><a href="javascript:void(0)">18:00</a></div><div class="timeList "><a href="javascript:void(0)">18:30</a></div><div class="timeList "><a href="javascript:void(0)">19:00</a></div><div class="timeList "><a href="javascript:void(0)">19:30</a></div><div class="timeList "><a href="javascript:void(0)">20:00</a></div><div class="timeList "><a href="javascript:void(0)">20:30</a></div><div class="timeList "><a href="javascript:void(0)">21:00</a></div><div class="timeList "><a href="javascript:void(0)">21:30</a></div><div class="timeList "><a href="javascript:void(0)">22:00</a></div>                                    </div>
                    <hr> -->
                    <a href="http://bmnkita.id/amara/pemesanan/create-pemesanan/R1?tanggal=2021-03-18"
                        class="btn btn-primary btn-sm"><i class="bi bi-cart-plus"></i>
                        Pesan Ruang Ini
                    </a>
                </div>
            </div>
            <br>
            <hr>
            <br>
        @endforeach
    </div>
</section>
</main>


    @extends('layout/footer')

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
                class="bi bi-arrow-up-short"></i></a>
    <!-- Uncomment below i you want to use a preloader -->
    <div id="preloader"></div>

    <script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/aos/aos.js" type="text/javascript"></script><script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/bootstrap-5/js/bootstrap.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/waypoints/noframework.waypoints.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/plugin/sweetalert2/sweetalert2.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/js/main.js" type="text/javascript"></script></body>
    </html>

