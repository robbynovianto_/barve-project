@extends('layout/navbar2')
<!-- CSS -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
</style>
    <title>Barve - Admin</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/aos.css">
        {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
        <link rel="stylesheet" href="../assets/css/bootstrap.min_2.css">
        <link rel="stylesheet" href="../assets/css/bootstrap-min.css">
        <link rel="stylesheet" href="../assets/css/sweetalert2.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/vendor.bundle.base.css">
<!-- Slider -->
        <section id="hero">
            <div class="hero-container">
                <section>
                    <div id="carouselID" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselIDindicators" data-bs-slide-to="0"
                                class="active"></button>
                            <button type="button" data-bs-target="#carouselIDindicators" data-bs-slide-to="1"
                                aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselIDindicators" data-bs-slide-to="2"
                                aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active"
                                style="background-image: url(../assets/img/slide1.jpg)">
                                <div class="carousel-container">
                                    <div class="container">
                                        <h2 class="animate__animated animate__fadeInDown mb-0">BARVE</h2>
                                        <p class="lead animate__animated animate__fadeInUp mb-2">Aplikasi Manajemen
                                            Rapat</p>
                                        <p class="animate__animated animate__fadeInUp">Mempermudah unit kerja dalam
                                            pemesanan
                                            ruang rapat, update informasi ketersediaan ruang rapat dan kelengkapannya
                                            serta real
                                            time jadwal pelaksanaan rapat di lingkungan sekretariat </p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item"
                                style="background-image: url(../assets/img/slide2.jpg)">
                                <div class="carousel-container">
                                    <div class="container">
                                        <h2 class="animate__animated animate__fadeInDown">Presensi Online</h2>
                                        <p class="animate__animated animate__fadeInUp">BARVE memiliki fitur pesan ruangan
                                            untuk memudahkan penyelenggara rapat dalam melakukan pertemuan </p>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item"
                                style="background-image: url(../assets/img/slide3.jpg)">
                                <div class="carousel-container">
                                    <div class="container">
                                        <h2 class="animate__animated animate__fadeInDown">Terintegrasi</h2>
                                        <p class="animate__animated animate__fadeInUp">BARVE kini telah terintegrasi
                                            dengan
                                            berbagai aplikasi lain di smartphone Anda sehingga semakin memudahkan Anda
                                            dalam
                                            mengakses informasi jadwal ruangan dan melakukan pemesanan</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselID"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselID"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                        </button>
                    </div>
                </section>
            </div>
        </section>
        <!-- Featured Services -->
        <section id="featured-services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 box">
                        <i class="bi bi-info-circle"></i>
                        <h4 class="title"><a href="">Info Ruang Meeting</a></h4>
                        <p class="description">Sekarang Anda sudah dapat melihat seluruh jadwal ruang meeting yang
                            tersedia
                            secara realtime dengan sangat mudah hanya dengan klik AMARA dari komputer maupun smartphone
                            Anda.</p>
                    </div>
                    <div class="col-lg-4 box box-bg">
                        <i class="bi bi-cart-check"></i>
                        <h4 class="title"><a href="">Pemesanan Ruangan Makin Mudah</a></h4>
                        <p class="description">Anda dapat melakukan pemesanan ruang meeting sesuai dengan spesifikasi,
                            kapasitas
                            dan waktu yang tersedia. Anda dapat melakukannya kapan pun dan dimana pun Anda sedang
                            berada.</p>
                    </div>
                    <div class="col-lg-4 box">
                        <i class="bi bi-hand-thumbs-up"></i>
                        <h4 class="title"><a href="">Proses Pemesanan 3 Langkah</a></h4>
                        <p class="description">Pemesanan ruang meeting hanya perlu 3 langkah. Anda hanya perlu pemesanan
                            ruangan, kemudian Admin akan melakukan persetujuan, dan terakhir Anda akan langsung mendapat
                            konfirmasi.</p>
                    </div>
                </div>
            </div>
        </section>

        <!-- About Us -->
        <section id="about">
            <div class="container" data-aos="fade-up">
                <header class="section-header">
                    <h3>Tentang BARVE</h3>
                    <p>
                        BARVE adalah sebuah aplikasi yang memudahkan Unit Kerja mendapatkan informasi ketersediaan ruang
                        rapat
                        <br>
                        <small><b>Dikelola oleh Tim Kerja Bagian Pengelolaan BMN dan Akomodasi, Biro Umum</b></small>
                    </p>
                </header>
                <div class="row about-cols">
                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                        <div class="about-col">
                            <div class="img">
                                <img src="assets/frontend/images/web/fitur-1.jpg" alt="" class="img-fluid">
                                <div class="icon"><i class="bi bi-calendar-week"></i></div>
                            </div>
                            <h2 class="title"><a href="#">Informasi Jadwal Ruangan</a></h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore
                                magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                aliquip ex ea
                                commodo consequat.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="about-col">
                            <div class="img">
                                <img src="assets/frontend/images/web/fitur-2.jpeg" alt="" class="img-fluid">
                                <div class="icon"><i class="bi bi-cart-check"></i></div>
                            </div>
                            <h2 class="title"><a href="#">Pesan Ruangan</a></h2>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium,
                                totam rem
                                aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                dicta sunt
                                explicabo.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-4" data-aos="fade-up" data-aos-delay="300">
                        <div class="about-col">
                            <div class="img">
                                <img src="assets/frontend/images/web/fitur-3.jpg" alt="" class="img-fluid">
                                <div class="icon"><i class="bi bi-clipboard-check"></i></div>
                            </div>
                            <h2 class="title"><a href="#">Absen Digital</a></h2>
                            <p>
                                Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores
                                eos qui
                                ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor
                                sit
                                amet.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
            @extends('layout/footer')

                <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
                        class="bi bi-arrow-up-short"></i></a>
                <!-- Uncomment below i you want to use a preloader -->
                <div id="preloader"></div>
            </body>

            </html>
