<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8"/>
        <meta name="robots" content="noindex, nofollow">
        <meta name="googlebot" content="noindex">
        <meta name="googlebot-news" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
            @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
        </style>
            <title>Barve - Admin</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
              rel="stylesheet">
              <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
              <link rel="stylesheet" href="../assets/css/animate.min.css">
              <link rel="stylesheet" href="../assets/css/aos.css">
              {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
              <link rel="stylesheet" href="../assets/css/bootstrap.min_2.css">
              <link rel="stylesheet" href="../assets/css/bootstrap-min.css">
              <link rel="stylesheet" href="../assets/css/sweetalert2.css">
              <link rel="stylesheet" href="../assets/css/custom.css">
              <link rel="stylesheet" href="../assets/css/materialdesignicons.min.css">
              <link rel="stylesheet" href="../assets/css/style.css">
              <link rel="stylesheet" href="../assets/css/vendor.bundle.base.css">
    <script>
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 100) {
                //clearHeader, not clearheader - caps H
                $("#header").addClass("header-scrolled");
            } else {
                $("#header").removeClass("header-scrolled");
            }
        }); //missing );
    </script>
    <body data-aos-easing="ease-in-out" data-aos-duration="1000" data-aos-delay="0"><script>
        var url = "#";
    </script>
<body data-aos-easing="ease-in-out" data-aos-duration="1000" data-aos-delay="0"><script>
    var url = "#";
</script>
<header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-center justify-content-between">
                <h1 class="logo"><a href="{{url ('/admin/home')}}">BARVE</a></h1>
                <nav id="navbar" class="navbar">
                    <ul>
                        <li>
                            <a class="nav-link " href="{{url ('/admin/home')}}">Beranda</a>
                        </li>

                        <li>
                            <a class="nav-link " href="{{url ('/admin/about')}}">Tentang</a>
                        </li>

                        <li>
                            <a class="nav-link " href="{{url ('/admin/jadwal')}}">Jadwal Harian</a>
                        </li>

                        <li>
                            <a class="nav-link " href="{{url ('/admin/pesan')}}">Pesan Ruangan</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{url ('/admin/dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{route('logout')}}">Keluar <i class="bi bi-box-arrow-right"></i></a>
                        </li>
                    </ul>
                    <i class="bi bi-list mobile-nav-toggle"></i>
                </nav>
            </div>
        </div>
    </div>
</header>
<main id="main">
    <div class="pageHeader">
        <div class="pageHeaderLayer">
            <div class="container">
                <div class="profile">
                    <div class="row">
                        <div class="col-md-1">
                            <img class="my-profile" src="http://bmnkita.id/amara//images/user/1613804383_89f98bd11a451d72af3f.jpg">
                        </div>
                    <div class="col-md-10">
                        <h4>Basuki Tafif</h4>
                        <p class="lead">Bagian Pengelolaan Barang Milik Negara</p>
                        <p>Terakhir login, 12 March 2021 01:56</p>
                    </div>
                </div>
                <button class="navbar-toggler d-md-none collapsed btn-warning btn-sm mb-2" type="button"
                        data-bs-toggle="collapse" data-bs-target="#sidebarMenu"
                        aria-controls="sidebarMenu"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span> MENU SEMENTARA
                </button>
            </div>
        </div>
    </div>
</div>

<style>
    .card-profile-image {
        padding: 30px;
        background-color: #e0e4ee;
    }

    .card-profile-image img {
        margin: 0 auto 5px;
        display: block;
    }

    .menu-link-a {
        text-decoration: none;
        color: inherit;
        display: block;
    }

    .menu-nav {
        padding: 10px;
    }


</style>
<section class="admin pt-4">
    <div class="container card">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse" style="">
                <div class="position-sticky">
                    <p><strong>MENU APLIKASI</strong></p>
                    <br>
                    <ul class="nav flex-column">
                        <li class="nav-item ">
                            <a class="nav-link" href="{{url ('/admin/dashboard')}}" style="font-weight: ">
                            <i class="bi bi-compass"></i>&nbsp; Dashboard</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link " href="#" style="font-weight: ">
                            <i class="bi bi-clipboard-check"></i>&nbsp; Pemesanan</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link " href="#" style="font-weight: ">
                            <i class="bi bi-briefcase"></i>&nbsp; Rapat</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link active" href="{{url ('/admin/ruangan')}}" style="font-weight: bold ">
                            <i class="bi bi-house"></i>&nbsp; Ruangan</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link " href="#" style="font-weight: ">
                            <i class="bi bi-people"></i>&nbsp; Pegawai</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link " href="#" style="font-weight: ">
                            <i class="bi bi-diagram-3"></i>&nbsp; Unit</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link " href="#" style="font-weight: ">
                            <i class="bi bi-gear"></i>&nbsp; Pengaturan</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link " href="#" style="font-weight: ">
                            <i class="bi bi-person-circle"></i>&nbsp; Akun</a>
                            </li>
                    </ul>
                </div>
            </nav>
            
            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 adminContent"><link href="#" rel="stylesheet" type="text/css" />
            <div class="row">
            <p class="text-muted font-13 m-b-30">
                <div class="col-12">
                <button type="button" class="btn btn-success" data-toggle ="modal" data-target="#tambah_ruangan">Tambah Data</button>
                    <div class="text-center">
                        <h1><strong>DAFTAR RUANGAN</strong></h1>
                        <table class="table ">
                            <thead class="table-dark">
                            <tr width="60%">
                                <th width="10%">No</th>
                                <th width="25%">Nama Ruangan</th>
                                <th width="25%">Kapasitas</th>
                                <th width="30%">Fasilitas</th>
                                <th width="5%">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ruangan as $r)
                            <tr width="60%">
                                <td width="10%">{{$r->id}}</td>
                                <td width="25%">{{$r->nama_ruangan}}</td>
                                <td width="25%">{{$r->kapasitas}}</td>
                                <td width="30%">
                                        @php($fasilitas = explode(".",$r->fasilitas))
                                        @foreach($fasilitas as $fas)
                                        {{$fas}}
                                        @endforeach 
                                </td>
                                <td width="25%">
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            Aksi
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                            <li><a class="dropdown-item" href="#">Lihat</a></li>
                                            <li><a class="dropdown-item" href="#">Edit</a></li>
                                            <li><a class="dropdown-item" href="#">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
</main>
<!-- Footer -->
@extends('layout/footer')

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<div id="preloader"></div>

<div class="modal fade" id="edit_pegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Project</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="post" id="form_edit" action="">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Nama Project</label>
                                        <div class="col-10">
                                            <input type="text" id="edit_nama_project" name="nama_project" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Anggota 1</label>
                                        <div class="col-10">
                                            <select class="form-control" id="edit_anggota1" name="anggota1">
                                                <option value="" disabled selected>Pilih Nama Anggota</option>
                                                @foreach($pegawai as $pg)
                                                <option value="{{$pg->nama}}">{{$pg->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Anggota 2</label>
                                        <div class="col-10">
                                            <select class="form-control" id="edit_anggota2" name="anggota2">
                                                <option value="" disabled selected>Pilih Nama Anggota</option>
                                                @foreach($pegawai as $pg)
                                                <option value="{{$pg->nama}}">{{$pg->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Anggota 3</label>
                                        <div class="col-10">
                                            <select class="form-control" id="edit_anggota3" name="anggota3">
                                                <option value="" disabled selected>Pilih Nama Anggota</option>
                                                @foreach($pegawai as $pg)
                                                <option value="{{$pg->nama}}">{{$pg->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Anggota 4</label>
                                        <div class="col-10">
                                            <select class="form-control" id="edit_anggota4" name="anggota4">
                                                <option value="" disabled selected>Pilih Nama Anggota</option>
                                                @foreach($pegawai as $pg)
                                                <option value="{{$pg->nama}}">{{$pg->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Anggota 5</label>
                                        <div class="col-10">
                                            <select class="form-control" id="edit_anggota5" name="anggota5">
                                                <option value="" disabled selected>Pilih Nama Anggota</option>
                                                @foreach($pegawai as $pg)
                                                <option value="{{$pg->nama}}">{{$pg->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Tanggal Mulai</label>
                                        <div class="col-10">
                                            <input class="form-control" id="edit_tanggal_mulai" name="tanggal_mulai" type="date" name="date">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Tanggal Selesai</label>
                                        <div class="col-10">
                                            <input class="form-control" id="edit_tanggal_selesai" name="tanggal_selesai" type="date" name="date">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Anggaran</label>
                                        <div class="col-10">
                                            <input type="text" id="edit_anggaran" name="anggaran" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Keterangan</label>
                                        <div class="col-10">
                                            <textarea type="text" id="edit_keterangan" name="keterangan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" id="done">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
<script src="../assets/js/aos.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery-3.5.1.min.js"></script>
<script src="../assets/js/main.js"></script>
<script src="../assets/js/noframework.waypoints.js"></script>
<script src="../assets/js/popper.min.js"></script>
<script src="../assets/js/sweetalert2.js"></script>
<script src="../assets/js/jquery.min.js"></script>
</html>
