<div class="modal fade" id="tambah_ruangan" tabindex="-1" role="dialog" aria-labelledby="tambahdata" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="post" action="/keuangan/store" enctype="multipart/form-data">
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Nama</label>
                                        <div class="col-10">
                                            <input type="text" name="nama" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">NIP</label>
                                        <div class="col-10">
                                            <input type="text" name="nip" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Tempat Lahir</label>
                                        <div class="col-10">
                                            <input type="text" name="tempat_lahir" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Tanggal Lahir</label>
                                        <div class="col-10">
                                            <input class="form-control" name="tanggal_lahir" type="date" name="date">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Alamat</label>
                                        <div class="col-10">
                                            <textarea type="text" name="alamat" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Jabatan</label>
                                        <div class="col-10">
                                            <input type="text" name="jabatan" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Departemen</label>
                                        <div class="col-10">
                                            <select class="form-control" name="departemen">
                                                <option value="" disabled selected>Pilih Departemen</option>
                                                <option value="Departemen Produksi">Departemen Produksi</option>
                                                <option value="Departemen Marketing">Departemen Marketing</option>
                                                <option value="Departemen keuangan">Departemen keuangan</option>
                                                <option value="Departemen Logistik">Departemen Logistik</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">No Telepon</label>
                                        <div class="col-10">
                                            <input type="text" name="notelp" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Foto</label>
                                        <div class="col-10">
                                            <input type="file" name="nama" class="form-control">
                                        </div>
                                    </div>
                                </div>
</div>
</div>