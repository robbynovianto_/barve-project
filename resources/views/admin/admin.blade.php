@extends('layout/navbar2')
<!-- CSS -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
</style>
    <title>Barve - Admin</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/aos.css">
        {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
        <link rel="stylesheet" href="../assets/css/bootstrap.min_2.css">
        <link rel="stylesheet" href="../assets/css/bootstrap-min.css">
        <link rel="stylesheet" href="../assets/css/sweetalert2.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/vendor.bundle.base.css">
<main id="main">
    <div class="pageHeader">
        <div class="pageHeaderLayer">
            <div class="container">
                <div class="profile">
                    <div class="row">
                        <div class="col-md-1">
                            <img class="my-profile" src="#">
                        </div>
                        <div class="col-md-10">
                            <h4>Basuki Tafif</h4>
                            <p class="lead">Bagian Pengelolaan Barang Milik Negara</p>
                            <p>Terakhir login, 12 March 2021 01:56</p>
                        </div>
                    </div>
                    <button class="navbar-toggler d-md-none collapsed btn-warning btn-sm mb-2" type="button"
                            data-bs-toggle="collapse" data-bs-target="#sidebarMenu"
                            aria-controls="sidebarMenu"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span> MENU SEMENTARA
                    </button>
                </div>
            </div>
        </div>
    </div>
<style>
    .card-profile-image {
        padding: 30px;
        background-color: #e0e4ee;
    }

    .card-profile-image img {
        margin: 0 auto 5px;
        display: block;
    }

    .menu-link-a {
        text-decoration: none;
        color: inherit;
        display: block;
    }

    .menu-nav {
        padding: 10px;
    }


</style>

<script src="http://bmnkita.id/amara/assets/js/function.js" type="text/javascript"></script><script>
    var backend_url = "http://bmnkita.id/amara/backend/";
</script>


<section class="admin pt-4">
    <div class="container card">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse" style="">
                <div class="position-sticky">
                    <p><strong>MENU APLIKASI</strong></p>
                    <br>
                    <ul class="nav flex-column">
                                                    <li class="nav-item ">
                                <a class="nav-link active" href="/admin" style="font-weight: bold">
                                    <i class="bi bi-compass"></i>
                                    &nbsp; Dashboard                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="#" style="font-weight: ">
                                    <i class="bi bi-clipboard-check"></i>
                                    &nbsp; Pemesanan                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="#" style="font-weight: ">
                                    <i class="bi bi-briefcase"></i>
                                    &nbsp; Rapat                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="{{url ('admin/ruangan')}}" style="font-weight: ">
                                    <i class="bi bi-house"></i>
                                    &nbsp; Ruangan                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="#" style="font-weight: ">
                                    <i class="bi bi-people"></i>
                                    &nbsp; Pegawai                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="#" style="font-weight: ">
                                    <i class="bi bi-diagram-3"></i>
                                    &nbsp; Unit                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="#" style="font-weight: ">
                                    <i class="bi bi-gear"></i>
                                    &nbsp; Pengaturan                                </a>
                            </li>
                                                        <li class="nav-item ">
                                <a class="nav-link " href="#" style="font-weight: ">
                                    <i class="bi bi-person-circle"></i>
                                    &nbsp; Akun                                </a>
                            </li>
                                                </ul>
                </div>
            </nav>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 adminContent"><link href="http://bmnkita.id/amara/assets/frontend/assets/css/dashboard.css" rel="stylesheet" type="text/css" />

<div class="row mt-5">
    <div class="col-12">
        <div class="text-center">
            <h1><strong>AMARA</strong></h1>
            <p class="lead">Aplikasi Manajemen Rapat</p>
            <p>

                AMARA (Aplikasi Manajemen Rapat) memudahkan Unit Kerja mendapatkan informasi ketersediaan ruang rapat di
                lingkungan Sekretariat Jendral MPR RI secara Real Time

            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card2 bg-c-blue order-card2">
            <div class="card2-block">
                <h6 class="m-b-20">Pemesanan Ruangan</h6>
                                <h2 class="text-right"><span>30</span></h2>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card2 bg-c-green order-card2">
            <div class="card2-block">
                <h6 class="m-b-20">Jadwal Selanjutnya</h6>
                                <h2 class="text-right">
                    <span>-</span>
                </h2>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card2 bg-c-yellow order-card2">
            <div class="card2-block">
                <h6 class="m-b-20">Online Terakhir Pada : </h6>
                <h2 class="text-right"><i
                            class="fa fa-refresh f-left"></i><span>12 March 2021</span>
                </h2>
            </div>
        </div>
    </div>

</div>

<div class="row" style="display: none">
    <div class="col-4">
        <div class="card2 mb-3 shadow-sm border-primary">
            <div class="card2-header">
                <h4 class="my-0 font-weight-normal">Terakhir Online</h4>
            </div>
            <div class="card2-body">
                <h1 class="card2-title">6 <small class="text-muted">hari lalu</small></h1>
                <p></p>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card2 mb-3 shadow-sm border-warning">
            <div class="card2-header">
                <h4 class="my-0 font-weight-normal">Total Pemesanan</h4>
            </div>
            <div class="card2-body">
                                <h1 class="card2-title">30 <small class="text-muted">kali</small></h1>
                <p>22 February 2021 s/d 02 December 2020</p>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card2 mb-3 shadow-sm border-danger">
            <div class="card2-header">
                <h4 class="my-0 font-weight-normal">Jadwal Selanjutnya</h4>
            </div>
            <div class="card2-body">
                                <h1 class="card2-title pricing-card2-title"> 30                    <small class="text-muted"> pemesanan</small>
                </h1>
                <p> </p>
            </div>
        </div>
    </div>
</div>
<br>
</div>
</div>
</div>
</section></main>
<!-- Footer -->
@extends('layout/footer')

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<div id="preloader"></div>

<script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/aos/aos.js" type="text/javascript"></script><script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/bootstrap-5/js/bootstrap.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/waypoints/noframework.waypoints.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/plugin/sweetalert2/sweetalert2.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/js/main.js" type="text/javascript"></script></body>
</html>

