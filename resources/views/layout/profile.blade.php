<main id="main">
    <div class="pageHeader">
        <div class="pageHeaderLayer">
            <div class="container">
                <div class="profile">
                    <div class="row">
                        <div class="col-md-1">
                            <img class="my-profile" src="#">
                        </div>
                        <div class="col-md-10">
                            <h4>Basuki Tafif</h4>
                            <p class="lead">Bagian Pengelolaan Barang Milik Negara</p>
                            <p>Terakhir login, 12 March 2021 01:56</p>
                        </div>
                    </div>
                    <button class="navbar-toggler d-md-none collapsed btn-warning btn-sm mb-2" type="button"
                            data-bs-toggle="collapse" data-bs-target="#sidebarMenu"
                            aria-controls="sidebarMenu"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span> MENU SEMENTARA
                    </button>
                </div>
            </div>
        </div>
    </div>
<style>
    .card-profile-image {
        padding: 30px;
        background-color: #e0e4ee;
    }

    .card-profile-image img {
        margin: 0 auto 5px;
        display: block;
    }

    .menu-link-a {
        text-decoration: none;
        color: inherit;
        display: block;
    }

    .menu-nav {
        padding: 10px;
    }


</style>
