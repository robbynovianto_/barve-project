    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>BARVE</strong>. All Rights Reserved
            </div>
            <div class="credits">
                Designed by <a href="www.instagram.com/robbynovianto">SIBANTER</a>
            </div>
        </div>
    </footer>
