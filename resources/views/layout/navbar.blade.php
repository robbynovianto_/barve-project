<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex">
    <meta name="googlebot-news" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
    </style>
    <title>Barve - Home</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/aos.css">
    {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
    <link rel="stylesheet" href="assets/css/bootstrap.min_2.css">
    <link rel="stylesheet" href="assets/css/bootstrap-min.css">
    <link rel="stylesheet" href="assets/css/sweetalert2.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/vendor.bundle.base.css">
</head>
<script>
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            //clearHeader, not clearheader - caps H
            $("#header").addClass("header-scrolled");
        } else {
            $("#header").removeClass("header-scrolled");
        }
    }); //missing );
</script>

<body data-aos-easing="ease-in-out" data-aos-duration="1000" data-aos-delay="0">
    <header id="header" class="fixed-top d-flex align-items-center header-transparent">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <h1 class="logo"><a href="{{url ('/')}}">BARVE</a></h1>
                    <nav id="navbar" class="navbar">
                        <ul>
                            <li>
                                <a class="nav-link " href="{{url ('/')}}">Beranda </a>
                            </li>
                            <li>
                                <a class="nav-link " href="{{url ('/about')}}">Tentang </a>
                            </li>
                            <li>
                                <a class="nav-link " href="{{url ('/jadwal')}}">Jadwal Harian </a>
                            </li>
                            <li>
                                <a class="nav-link " href="{{url ('/ruangan')}}">Pesan Ruangan </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{url ('/login')}}">Masuk</a>
                            </li>
                        </ul>
                        <i class="mobile-nav-toggle bi bi-list"></i>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <main id="main">

    <script src="assets/js/aos.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/noframework.waypoints.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/sweetalert2.js"></script>