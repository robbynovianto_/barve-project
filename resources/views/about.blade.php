@extends('layout/navbar')
<!-- CSS -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
</style>
    <title>Barve - Admin</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/aos.css">
        {{-- <link rel="stylesheet" href="assets/css/bootstrap-icons.css"> --}}
        <link rel="stylesheet" href="../assets/css/bootstrap.min_2.css">
        <link rel="stylesheet" href="../assets/css/bootstrap-min.css">
        <link rel="stylesheet" href="../assets/css/sweetalert2.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
        <link rel="stylesheet" href="../assets/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/vendor.bundle.base.css">
<!-- Slider -->
<main id="main">
    <div class="pageHeader">
        <div class="pageHeaderLayer">
            <div class="container">
                <h1>Tentang BARVE</h1>
                <p>
                BARVE merupakan aplikasi untuk memudahkan Unit Kerja mendapatkan informasi ketersediaan ruang rapat di
                lingkungan Sekretariat.
                <br>
                <small><b>Dikelola oleh Tim Kerja Bagian Pengelolaan BMN dan Akomodasi, Biro Umum</b></small></p>
            </div>
        </div>
    </div>

<!-- About Us -->
<section id="about">
    <div class="container" data-aos="zoom-in">
        <div class="row about-cols">
            <div class="col-md-4" data-aos="fade-right" data-aos-delay="100">
                <div class="about-col">
                    <div class="img">
                        <img src="assets/frontend/images/web/fitur-1.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="bi bi-calendar-week"></i></div>
                    </div>
                    <h2 class="title"><a href="#">Informasi Jadwal Ruangan</a></h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                </div>
            </div>

            <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                <div class="about-col">
                    <div class="img">
                        <img src="assets/frontend/images/web/fitur-2.jpeg" alt="" class="img-fluid">
                        <div class="icon"><i class="bi bi-cart-check"></i></div>
                    </div>
                    <h2 class="title"><a href="#">Pesan Ruangan</a></h2>
                    <p>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>

            <div class="col-md-4" data-aos="fade-left" data-aos-delay="300">
                <div class="about-col">
                    <div class="img">
                        <img src="assets/frontend/images/web/fitur-3.jpg" alt="" class="img-fluid">
                        <div class="icon"><i class="bi bi-clipboard-check"></i></div>
                    </div>
                    <h2 class="title"><a href="#">Absen Digital</a></h2>
                    <p>
                        Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Fitur -->
<section id="services">
    <div class="container aos-init aos-animate" data-aos="fade-up">
        <header class="section-header wow fadeInUp">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <h3>FITUR</h3>
                    <p>AMARA memiliki sejumlah fitur yang keren dan sangat bermanfaat dalam menunjang aktifitas Anda.</p>
                </div>
            </div>
        </header>
        <div class="row">
            <div class="col-lg-4 col-md-6 box aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                <div class="icon"><i class="bi bi-calendar2-check"></i></div>
                <h4 class="title"><a href="">Layanan 24/7</a></h4>
                <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>
            <div class="col-lg-4 col-md-6 box aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                <div class="icon"><i class="bi bi-phone"></i></div>
                <h4 class="title"><a href="">Mobile Friendly</a></h4>
                <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div>
            <div class="col-lg-4 col-md-6 box aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
                <div class="icon"><i class="bi bi-clock"></i></div>
                <h4 class="title"><a href="">Realtime Information</a></h4>
                <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
            </div>
            <div class="col-lg-4 col-md-6 box aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
                <div class="icon"><i class="bi bi-ui-checks-grid"></i></div>
                <h4 class="title"><a href="">Easy To Use</a></h4>
                <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
            </div>
            <div class="col-lg-4 col-md-6 box aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                <div class="icon"><i class="bi bi-bounding-box"></i></div>
                <h4 class="title"><a href="">Terintegrasi</a></h4>
                <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
            <div class="col-lg-4 col-md-6 box aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
                <div class="icon"><i class="bi bi-journal-bookmark"></i></div>
                <h4 class="title"><a href="">Arsip Riwayat</a></h4>
                <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact" class="section-bg">
    <div class="container" data-aos="fade-up">
        <div class="section-header">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <h3>Kontak</h3>
                    <p>Untuk informasi selanjutnya, Anda dapat mengirimkan pertanyaan ke kami melalui informasi kontak di bawah ini</p>
                </div>
            </div>
        </div>
        <div class="row contact-info">
            <div class="col-md-4">
                <div class="contact-address">
                    <i class="bi bi-geo-alt"></i>
                    <h3>Alamat</h3>
                    <address>Kominfo Magetan, Jawa Tengah, Indonesia</address>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-phone">
                    <i class="bi bi-phone"></i>
                    <h3>Nomor Telepon</h3>
                    <p><a href="tel:+6221234567">+62 877 307 996 9</a></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-email">
                    <i class="bi bi-envelope"></i>
                    <h3>Email</h3>
                    <p><a href="mailto:info@amara.com">barve@gmail.com</a></p>
                </div>
            </div>
        </div>

        <div class="form">
            <form action="#" method="post" role="form" class="php-email-form">
                <div class="row">
                    <div class="form-group col-md-6">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Nama Lengkap" required>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Alamat Email" required>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Judul Pesan" required>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" placeholder="Isi Pesan" required></textarea>
                </div>
                <div class="my-3">
                    <div class="loading">Loading</div>
                    <div class="error-message"></div>
                    <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit">Kirim Pesan</button></div>
            </form>
        </div>
    </div>
</section></main>
@extends('layout/footer')

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<div id="preloader"></div>

<script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/aos/aos.js" type="text/javascript"></script><script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/bootstrap-5/js/bootstrap.min.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/vendor/waypoints/noframework.waypoints.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/plugin/sweetalert2/sweetalert2.js" type="text/javascript"></script><script src="http://bmnkita.id/amara/assets/frontend/assets/js/main.js" type="text/javascript"></script></body>
</html>

