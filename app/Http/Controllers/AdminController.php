<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ruangan;

class AdminController extends Controller
{
    // Agar tidak asal masuk
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {
        return view('admin/admin');
    }

    public function index()
    {
        return view('admin/home');
    }

    public function about()
    {
        return view('admin/about');
    }

    public function ruangan()
    {   
        $ruangan = Ruangan::all();
        return view('admin/ruangan', compact('ruangan'));
    }
}
